// CRUD Operations

/*
	C - Create (Insert document)
	R - Read/Retrieve (View Specific Document)
	U - Update (Edit specific document)
	D - Delete (Remove specific)

	- CRUD 
*/

// [SECTION] Insert a document (Create)

/*
	- Syntax:
		- bd.collectionName.insertOne({object});

	Comparison with javascript
		object.object.method({object});
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact:{
		phone: "87654321",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});


/*
	Mini Activity

            Scenario: We will create a database that will simulate a hotel database.
            1. Create a new database called "hotel".
            2. Insert a single room in the "rooms" collection with the following details:
                
                name - single
                accommodates - 2
                price - 1000
                description - A simple room with basic necessities
                rooms_available - 10
                isAvailable - false

            3. Use the "db.getCollection('users').find({})" query to check if the document is created.
            
            4. Take a screenshot of the Robo3t result and send it to the batch hangouts.
*/

db.rooms.insertOne({
	name : "single",
    accommodates : 2,
    price - 1000,
    description : "A simple room with basic necessities",
    rooms_available : 10,
    isAvailable : false
});

	// Insert many
	/*
		
	*/

	db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact:{
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 80,
		contact:{
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}
	])
	

/*

*/

db.rooms.insertMany([
{
	name : "double",
    accomodates : 3,
    price : 2000,
    description : "A room fit for a small family going on a vacation",
    rooms_available : 5,
    isAvailable : false
},
{
	name : "queen",
    accommodates : 4,
    price : 4000,
    description : "A room with a queen sized bed perfect for a simple getaway",
    rooms_available : 15,
    isAvailable : false
}
]);

// [SECTION] Retrieve a document/s (Read)

/*
	Syntax:
		- db.collection.find({}); // get all documents
		- db.collection.find({field:value}); // get a specific document
*/

// Find all the documents in the collection
db.users.find({});

// Find a spepcific document in the collection using the field value.
db.users.find({firstName: "Stephen"});
db.users.find({department: "none"});

// Find documents with multiple parameters.
/*
	Syntax:
		- db.collectionName.find({fieldA:valueA, fieldB:valueB}); 
*/

db.users.find({lastName: "Armstrong", age: 82});


// [SECTION] Updating documents (Read)
// Create a document to update
db.users.insertOne({
	firstName: "Test",
	lastName: "Test", 
	age: 0,
	contact: {
		phone: "00000",
		email: "testing@gmail.com"
	},
	courses: [],
	department: ""
});
/*
	Syntax:
		- db.collectionName.updateOne({criteria}, {$set:{field:value}});
*/

	db.users.updateOne(
		{firstName: "Test"},
		{
			$set:{
				firstName: "Bill",
				lastName: "Gates", 
				age: 65,
				contact: {
					phone: "12345678",
					email: "bill@gmail.com"
				},
				courses: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "active"
			}
		}
	)

	db.users.updateMany(
		{department: "none"},
		{
			$set:{department: "Operations"}
		}
	);

	// Replace One
	/*
		Syntax:
			- db.collectionName.replaceOne({criteria}, {$set: {field: value}});
	*/

	db.users.replaceOne(
		{firstName: "Bill"},
		{
			firstName: "Bill",
			lastName: "Gates", 
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	)

	db.rooms.updateOne(
		{name: "queen"},
		{
			$set:{rooms_available: 0}
		}
	);

	db.rooms.find({
		name: "queen"
	});

	db.users.deleteOne({
		firstName: "Test"
	});


	db.rooms.deleteMany({
		rooms_available: 0
	});

	db.users.find({
		contact:{
		phone: "87654321",
		email: "stephenhawking@gmail.com"
		}
	});

